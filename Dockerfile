FROM ubuntu:14.04
LABEL maintainer "fahmi.akbar.w@mail.ugm.ac.id"

# setup environment
RUN locale-gen en_US.UTF-8
ENV LANG en_US.UTF-8
ARG DEBIAN_FRONTEND=noninteractive
ENV ROS_DISTRO indigo

RUN apt-get update \
&& apt-get install -y \
        software-properties-common build-essential \
        wget curl git pkg-config unzip

RUN add-apt-repository ppa:v-launchpad-jochen-sprickerhof-de/pcl \
&& wget http://packages.ros.org/ros.key -O - | apt-key add - \
&& sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'

# Intall ROS INDIGO
RUN apt-get update && apt-get install -y \
        cmake \
        python3-pip python3-dev python3.4-venv \
        python-dev python-pip \
        libswscale-dev \
        libtbb2 \
        libtbb-dev \
        libatlas-base-dev libatlas3gf-base gfortran \
        liblapack-dev liblapacke-dev \
        ros-$ROS_DISTRO-ros-base \
        ros-$ROS_DISTRO-image-pipeline \
        ros-$ROS_DISTRO-laser-pipeline \
&& python3 -m pip install --upgrade pip wheel \
&& python3 -m pip install --upgrade rosdep rosinstall vcstools \
&& rosdep init \
&& rosdep update \
&& rm -rf /var/lib/apt/lists/* /tmp/* $HOME/.cache/pip/http/* $HOME/.cache/pip/wheels/* \
&& rm -r /usr/lib/python3/dist-packages/__pycache__

# TODO : use pyenv to switch python 3.6.2
ENV PYTHONPATH /opt/ros/indigo/lib/python2.7/dist-packages/:/usr/lib/python3/dist-packages/
# TODO : ENV PYTHONPATH=/opt/ros/$(rosversion -d)/lib/python2.7/dist-packages/:$(python3 -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())

# Install tensorflow
RUN python3 -m pip install --upgrade numpy six \
--target=$(python3 -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())") \
&& python3 -m pip install --upgrade -r \
        https://raw.githubusercontent.com/TensorVision/TensorVision/master/requirements.txt \
&& rm -rf /tmp/* $HOME/.cache/pip/http/* $HOME/.cache/pip/wheels/* \
&& rm -r /usr/lib/python3/dist-packages/__pycache__

RUN python3 -m pip install \
    https://storage.googleapis.com/tensorflow/linux/cpu/tensorflow-1.0.1-cp34-cp34m-linux_x86_64.whl \
    https://github.com/TensorVision/TensorVision/archive/master.zip \
&& rm -rf /tmp/* $HOME/.cache/pip/http/* $HOME/.cache/pip/wheels/* \
&& rm -r /usr/lib/python3/dist-packages/__pycache__

# Install OpenCV
RUN wget https://github.com/Itseez/opencv/archive/3.2.0.zip \
&& unzip 3.2.0.zip \
&& mkdir /opencv-3.2.0/cmake_binary \
&& cd /opencv-3.2.0/cmake_binary \
&& cmake -DBUILD_TIFF=ON \
        -DBUILD_opencv_java=OFF \
        -DWITH_CUDA=OFF \
        -DENABLE_AVX=ON \
        -DWITH_OPENGL=ON \
        -DWITH_OPENCL=ON \
        -DWITH_IPP=ON \
        -DWITH_TBB=ON \
        -DWITH_EIGEN=ON \
        -DWITH_V4L=ON \
        -DBUILD_TESTS=OFF \
        -DBUILD_PERF_TESTS=OFF \
        -DCMAKE_BUILD_TYPE=RELEASE \
        -DCMAKE_INSTALL_PREFIX=$(python3 -c "import sys; print(sys.prefix)") \
        -DPYTHON_EXECUTABLE=$(which python3) \
        -DPYTHON_INCLUDE_DIR=$(python3 -c "from distutils.sysconfig import get_python_inc; print(get_python_inc())") \
        -DPYTHON_PACKAGES_PATH=$(python3 -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())") .. \
&& make install -j $(grep -c ^processor /proc/cpuinfo) \
&& rm /3.2.0.zip \
&& rm -r /opencv-3.2.0

# # TODO: Compile PCL with GPU support 
# RUN wget https://github.com/PointCloudLibrary/pcl/archive/pcl-1.8.0.zip \
# && unzip pcl-1.8.0.zip \
# && mkdir /pcl-1.8.0/cmake_binary \
# && cd /pcl-1.8.0/cmake_binary \
# && cmake -DBUILD_GPU=ON .. \
# && make install -j $(grep -c ^processor /proc/cpuinfo) \
# && rm /pcl-1.8.0.zip \
# && rm -r /pcl-1.8.0

# RUN apt-get purge -y \
#     ros-indigo-image-view \
#     ros-indigo-cv-bridge \
#     ros-indigo-vision-opencv \
#     xbitmaps \
#     xorg-sgml-doctools \
#     xterm \
#     xtrans-dev \
#     qt4-qmake qtchooser qtcore4-l10n \
#     libopencv* \
#     x11* \
#     libqt* \
#     libvtk* \
#     libx11*


EXPOSE 11311

COPY ./ros_entrypoint.sh /
RUN chmod +x /ros_entrypoint.sh
ENTRYPOINT ["/ros_entrypoint.sh"]
CMD bash