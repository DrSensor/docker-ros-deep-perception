#!/bin/bash
set -e

# setup ros environment
source "/opt/ros/$ROS_DISTRO/setup.bash"
export PYTHONPATH=/opt/ros/indigo/lib/python2.7/dist-packages/:/usr/lib/python3/dist-packages/
export TERM=xterm

exec "$@"